# frozen_string_literal: true

require 'application_system_test_case'

class RoundsTest < ApplicationSystemTestCase
  setup do
    @round = rounds(:one)
  end

  test 'visiting the index' do
    visit rounds_url
    assert_selector 'h1', text: 'Rounds'
  end

  test 'creating a Round' do
    visit rounds_url
    click_on 'New Round'

    fill_in 'Address', with: @round.address
    fill_in 'Ended races counter', with: @round.ended_races_counter
    fill_in 'Max number of carts', with: @round.max_number_of_carts
    fill_in 'Name', with: @round.name
    fill_in 'Price', with: @round.price
    fill_in 'Season', with: @round.season_id
    fill_in 'Start time', with: @round.start_time
    fill_in 'Track name', with: @round.track_name
    click_on 'Create Round'

    assert_text 'Round was successfully created'
    click_on 'Back'
  end

  test 'updating a Round' do
    visit rounds_url
    click_on 'Edit', match: :first

    fill_in 'Address', with: @round.address
    fill_in 'Ended races counter', with: @round.ended_races_counter
    fill_in 'Max number of carts', with: @round.max_number_of_carts
    fill_in 'Name', with: @round.name
    fill_in 'Price', with: @round.price
    fill_in 'Season', with: @round.season_id
    fill_in 'Start time', with: @round.start_time
    fill_in 'Track name', with: @round.track_name
    click_on 'Update Round'

    assert_text 'Round was successfully updated'
    click_on 'Back'
  end

  test 'destroying a Round' do
    visit rounds_url
    page.accept_confirm do
      click_on 'Destroy', match: :first
    end

    assert_text 'Round was successfully destroyed'
  end
end
