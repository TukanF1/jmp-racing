# frozen_string_literal: true

require 'test_helper'

class RoundsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @round = rounds(:one)
  end

  test 'should get index' do
    get rounds_url
    assert_response :success
  end

  test 'should get new' do
    get new_round_url
    assert_response :success
  end

  test 'should create round' do
    assert_difference('Round.count') do
      post rounds_url, params: {
        round: {
          address: @round.address,
          ended_races_counter: @round.ended_races_counter,
          max_number_of_carts: @round.max_number_of_carts,
          name: @round.name, price: @round.price, season_id: @round.season_id,
          start_time: @round.start_time, track_name: @round.track_name
        }
      }
    end

    assert_redirected_to round_url(Round.last)
  end

  test 'should show round' do
    get round_url(@round)
    assert_response :success
  end

  test 'should get edit' do
    get edit_round_url(@round)
    assert_response :success
  end

  test 'should update round' do
    patch round_url(@round), params: {
      round: {
        address: @round.address,
        ended_races_counter: @round.ended_races_counter,
        max_number_of_carts: @round.max_number_of_carts, name: @round.name,
        price: @round.price, season_id: @round.season_id,
        start_time: @round.start_time, track_name: @round.track_name
      }
    }
    assert_redirected_to round_url(@round)
  end

  test 'should destroy round' do
    assert_difference('Round.count', -1) do
      delete round_url(@round)
    end

    assert_redirected_to rounds_url
  end
end
