# frozen_string_literal: true

Rails.application.routes.draw do
  resources :series do
    resources :seasons, except: [:index] do
      resources :rounds, except: [:index] do
        member do
          get 'starting_teams' => 'rounds#edit_teams'
          patch 'starting_teams' => 'rounds#update_teams'
          get 'qualification' => 'rounds#show_qualification'
          get 'qualification_group_result/:qualification_group_result_id' => 'rounds#edit_qualification_group_result', as: :qualification_group_result
          patch 'qualification_group_result/:qualification_group_result_id' => 'rounds#update_qualification_group_result'
          get 'qualification_generate_result' => 'rounds#generate_qualification_result'
          get 'qualification_result/:qualification_result_id' => 'rounds#edit_qualification_result', as: :qualification_result
          patch 'qualification_result/:qualification_result_id' => 'rounds#update_qualification_result'
          get 'races' => 'rounds#show_races'
          get 'race_result/:race_result_id' => 'rounds#edit_race_result', as: :race_result
          patch 'race_result/:race_result_id' => 'rounds#update_race_result'
          get 'carts' => 'rounds#show_carts'
          get 'cart/:cart_id' => 'rounds#edit_cart', as: :cart
          patch 'cart/:cart_id' => 'rounds#update_cart'
          delete 'cart/:cart_id' => 'rounds#destroy_cart'
          get 'new_cart' => 'rounds#new_cart', as: :new_cart
          post 'new_cart' => 'rounds#create_cart'
          get 'results' => 'rounds#show_results'
        end
      end
      resources :teams, except: [:show]
    end
  end
  devise_for :users, skip: [:confirmation, :passwords, :registrations]
  root 'home#index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
