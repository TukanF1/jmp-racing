# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_01_20_214830) do

  create_table "carts", force: :cascade do |t|
    t.integer "number", null: false
    t.boolean "special", default: false, null: false
    t.integer "round_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["number", "round_id"], name: "index_carts_on_number_and_round_id", unique: true
    t.index ["round_id"], name: "index_carts_on_round_id"
  end

  create_table "qualification_group_results", force: :cascade do |t|
    t.integer "qualification_group_id", null: false
    t.integer "team_id", null: false
    t.time "time"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "cart_id"
    t.index ["cart_id"], name: "index_qualification_group_results_on_cart_id"
    t.index ["qualification_group_id"], name: "index_qualification_group_results_on_qualification_group_id"
    t.index ["team_id"], name: "index_qualification_group_results_on_team_id"
  end

  create_table "qualification_groups", force: :cascade do |t|
    t.string "name", null: false
    t.integer "qualification_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name", "qualification_id"], name: "index_qualification_groups_on_name_and_qualification_id", unique: true
    t.index ["qualification_id"], name: "index_qualification_groups_on_qualification_id"
  end

  create_table "qualification_results", force: :cascade do |t|
    t.integer "qualification_id", null: false
    t.integer "team_id", null: false
    t.integer "position"
    t.time "time"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["qualification_id"], name: "index_qualification_results_on_qualification_id"
    t.index ["team_id"], name: "index_qualification_results_on_team_id"
  end

  create_table "qualifications", force: :cascade do |t|
    t.string "name", null: false
    t.integer "round_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name", "round_id"], name: "index_qualifications_on_name_and_round_id", unique: true
    t.index ["round_id"], name: "index_qualifications_on_round_id"
  end

  create_table "race_group_teams", force: :cascade do |t|
    t.integer "race_group_id", null: false
    t.integer "team_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["race_group_id"], name: "index_race_group_teams_on_race_group_id"
    t.index ["team_id"], name: "index_race_group_teams_on_team_id"
  end

  create_table "race_groups", force: :cascade do |t|
    t.string "name", null: false
    t.integer "round_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name", "round_id"], name: "index_race_groups_on_name_and_round_id", unique: true
    t.index ["round_id"], name: "index_race_groups_on_round_id"
  end

  create_table "race_race_groups", force: :cascade do |t|
    t.integer "race_id", null: false
    t.integer "race_group_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["race_group_id"], name: "index_race_race_groups_on_race_group_id"
    t.index ["race_id"], name: "index_race_race_groups_on_race_id"
  end

  create_table "race_results", force: :cascade do |t|
    t.integer "race_id", null: false
    t.integer "team_id", null: false
    t.integer "position", default: 1, null: false
    t.time "time"
    t.decimal "points", default: "0.0", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "cart_id"
    t.integer "starting_position"
    t.index ["cart_id"], name: "index_race_results_on_cart_id"
    t.index ["race_id"], name: "index_race_results_on_race_id"
    t.index ["team_id"], name: "index_race_results_on_team_id"
  end

  create_table "races", force: :cascade do |t|
    t.string "name", null: false
    t.integer "round_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name", "round_id"], name: "index_races_on_name_and_round_id", unique: true
    t.index ["round_id"], name: "index_races_on_round_id"
  end

  create_table "round_results", force: :cascade do |t|
    t.integer "round_id", null: false
    t.integer "team_id", null: false
    t.integer "position"
    t.decimal "points"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["round_id"], name: "index_round_results_on_round_id"
    t.index ["team_id"], name: "index_round_results_on_team_id"
  end

  create_table "rounds", force: :cascade do |t|
    t.string "name", null: false
    t.string "track_name", null: false
    t.string "address", null: false
    t.datetime "start_time", null: false
    t.string "price", null: false
    t.integer "max_number_of_carts", default: 0, null: false
    t.integer "ended_races_counter", default: 0, null: false
    t.integer "season_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name", "season_id"], name: "index_rounds_on_name_and_season_id", unique: true
    t.index ["season_id"], name: "index_rounds_on_season_id"
  end

  create_table "seasons", force: :cascade do |t|
    t.string "name", null: false
    t.integer "ended_rounds_counter", default: 0, null: false
    t.integer "serie_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name", "serie_id"], name: "index_seasons_on_name_and_serie_id", unique: true
    t.index ["serie_id"], name: "index_seasons_on_serie_id"
  end

  create_table "series", force: :cascade do |t|
    t.string "name", null: false
    t.integer "ended_seasons_counter", default: 0, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_series_on_name", unique: true
  end

  create_table "team_rounds", force: :cascade do |t|
    t.integer "team_id", null: false
    t.integer "round_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["round_id"], name: "index_team_rounds_on_round_id"
    t.index ["team_id"], name: "index_team_rounds_on_team_id"
  end

  create_table "teams", force: :cascade do |t|
    t.string "name", null: false
    t.decimal "points", default: "0.0", null: false
    t.integer "season_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "special_cart", default: false, null: false
    t.index ["name", "season_id"], name: "index_teams_on_name_and_season_id", unique: true
    t.index ["season_id"], name: "index_teams_on_season_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "carts", "rounds"
  add_foreign_key "qualification_group_results", "qualification_groups"
  add_foreign_key "qualification_group_results", "teams"
  add_foreign_key "qualification_groups", "qualifications"
  add_foreign_key "qualification_results", "qualifications"
  add_foreign_key "qualification_results", "teams"
  add_foreign_key "qualifications", "rounds"
  add_foreign_key "race_group_teams", "race_groups"
  add_foreign_key "race_group_teams", "teams"
  add_foreign_key "race_groups", "rounds"
  add_foreign_key "race_race_groups", "race_groups"
  add_foreign_key "race_race_groups", "races"
  add_foreign_key "race_results", "races"
  add_foreign_key "race_results", "teams"
  add_foreign_key "races", "rounds"
  add_foreign_key "round_results", "rounds"
  add_foreign_key "round_results", "teams"
  add_foreign_key "rounds", "seasons"
  add_foreign_key "seasons", "series", column: "serie_id"
  add_foreign_key "team_rounds", "rounds"
  add_foreign_key "team_rounds", "teams"
  add_foreign_key "teams", "seasons"
end
