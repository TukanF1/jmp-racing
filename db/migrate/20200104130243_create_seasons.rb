class CreateSeasons < ActiveRecord::Migration[6.0]
  def change
    create_table :seasons do |t|
      t.string :name, null: false
      t.integer :ended_rounds_counter, null: false, default: 0
      t.belongs_to :serie, null: false, foreign_key: true
      
      t.index [:name, :serie_id], unique: true

      t.timestamps
    end
  end
end
