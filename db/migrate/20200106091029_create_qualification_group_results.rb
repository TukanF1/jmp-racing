class CreateQualificationGroupResults < ActiveRecord::Migration[6.0]
  def change
    create_table :qualification_group_results do |t|
      t.belongs_to :qualification_group, null: false, foreign_key: true
      t.belongs_to :team, null: false, foreign_key: true
      t.time :time

      t.timestamps
    end
  end
end
