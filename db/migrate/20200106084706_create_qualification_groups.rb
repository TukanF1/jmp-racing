class CreateQualificationGroups < ActiveRecord::Migration[6.0]
  def change
    create_table :qualification_groups do |t|
      t.string :name, null: false
      t.belongs_to :qualification, null: false, foreign_key: true
      
      t.index [:name, :qualification_id], unique: true

      t.timestamps
    end
  end
end
