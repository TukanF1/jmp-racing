class CreateRaceResults < ActiveRecord::Migration[6.0]
  def change
    create_table :race_results do |t|
      t.belongs_to :race, null: false, foreign_key: true
      t.belongs_to :team, null: false, foreign_key: true
      t.integer :position, null: false, default: 1
      t.time :time
      t.decimal :points, null: false, default: 0

      t.timestamps
    end
  end
end
