class CreateRaceGroups < ActiveRecord::Migration[6.0]
  def change
    create_table :race_groups do |t|
      t.string :name, null: false
      t.belongs_to :round, null: false, foreign_key: true
      
      t.index [:name, :round_id], unique: true

      t.timestamps
    end
  end
end
