class CreateTeams < ActiveRecord::Migration[6.0]
  def change
    create_table :teams do |t|
      t.string :name, null: false
      t.decimal :points, null: false, default: 0
      t.belongs_to :season, null: false, foreign_key: true
      
      t.index [:name, :season_id], unique: true

      t.timestamps
    end
  end
end
