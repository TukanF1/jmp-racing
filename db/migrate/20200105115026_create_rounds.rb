class CreateRounds < ActiveRecord::Migration[6.0]
  def change
    create_table :rounds do |t|
      t.string :name, null: false
      t.string :track_name, null: false
      t.string :address, null: false
      t.datetime :start_time, null: false
      t.string :price, null: false
      t.integer :max_number_of_carts, null: false, default: 0
      t.integer :ended_races_counter, null: false, default: 0
      t.belongs_to :season, null: false, foreign_key: true
      
      t.index [:name, :season_id], unique: true

      t.timestamps
    end
  end
end
