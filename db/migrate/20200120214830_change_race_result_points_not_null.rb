class ChangeRaceResultPointsNotNull < ActiveRecord::Migration[6.0]
  def change
    change_column :race_results, :points, :decimal, null: false, default: 0
    change_column :race_results, :position, :integer, null: false, default: 1
  end
end
