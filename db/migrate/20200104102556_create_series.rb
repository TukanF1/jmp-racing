class CreateSeries < ActiveRecord::Migration[6.0]
  def change
    create_table :series do |t|
      t.string :name, null: false
      t.integer :ended_seasons_counter, null: false, default: 0
      
      t.index :name, unique: true

      t.timestamps
    end
  end
end
