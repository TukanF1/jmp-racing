class CreateRoundResults < ActiveRecord::Migration[6.0]
  def change
    create_table :round_results do |t|
      t.belongs_to :round, null: false, foreign_key: true
      t.belongs_to :team, null: false, foreign_key: true
      t.integer :position
      t.decimal :points

      t.timestamps
    end
  end
end
