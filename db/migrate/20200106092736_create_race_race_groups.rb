class CreateRaceRaceGroups < ActiveRecord::Migration[6.0]
  def change
    create_table :race_race_groups do |t|
      t.belongs_to :race, null: false, foreign_key: true
      t.belongs_to :race_group, null: false, foreign_key: true

      t.timestamps
    end
  end
end
