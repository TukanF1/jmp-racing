class ChangeRaceResult < ActiveRecord::Migration[6.0]
  def change
    change_table(:race_results) do |t|
      t.belongs_to :cart, null: true
      t.integer :starting_position
    end
  end
end
