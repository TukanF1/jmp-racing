class CreateQualificationResults < ActiveRecord::Migration[6.0]
  def change
    create_table :qualification_results do |t|
      t.belongs_to :qualification, null: false, foreign_key: true
      t.belongs_to :team, null: false, foreign_key: true
      t.integer :position
      t.time :time

      t.timestamps
    end
  end
end
