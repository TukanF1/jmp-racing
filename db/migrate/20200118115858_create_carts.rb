class CreateCarts < ActiveRecord::Migration[6.0]
  def change
    create_table :carts do |t|
      t.integer :number, null: false
      t.boolean :special, null: false, default: :false
      t.belongs_to :round, null: false, foreign_key: true
      
      t.index [:number, :round_id], unique: true

      t.timestamps
    end
  end
end
