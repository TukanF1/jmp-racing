class ChangeTeam < ActiveRecord::Migration[6.0]
  def change
    change_table(:teams) do |t|
      t.boolean :special_cart, null: false, default: :false
    end
  end
end
