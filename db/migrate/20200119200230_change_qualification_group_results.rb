class ChangeQualificationGroupResults < ActiveRecord::Migration[6.0]
  def change
    change_table(:qualification_group_results) do |t|
      t.belongs_to :cart, null: true
    end
  end
end
