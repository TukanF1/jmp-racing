# frozen_string_literal: true

json.extract! round, :id, :name, :track_name, :address, :start_time, :price, :max_number_of_carts, :ended_races_counter, :season_id, :created_at, :updated_at
json.url round_url(round, format: :json)
