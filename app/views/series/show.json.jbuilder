# frozen_string_literal: true

json.partial! 'series/series', series: @series
