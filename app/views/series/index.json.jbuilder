# frozen_string_literal: true

json.array! @series, partial: 'series/series', as: :series
