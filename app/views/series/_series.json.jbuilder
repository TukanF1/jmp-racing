# frozen_string_literal: true

json.extract! series, :id, :name, :created_at, :updated_at
json.url series_url(series, format: :json)
