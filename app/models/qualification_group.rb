# frozen_string_literal: true

class QualificationGroup < ApplicationRecord
  belongs_to :qualification
  has_many :qualification_group_results, dependent: :destroy
end
