# frozen_string_literal: true

class RoundResult < ApplicationRecord
  belongs_to :round
  belongs_to :team
end
