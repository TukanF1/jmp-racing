# frozen_string_literal: true

class Round < ApplicationRecord
  validates_uniqueness_of :name, scope: :season
  validates :price, format: { with: /\A\d+(,\d{1,2})? [A-Z]{3}\z/,
    message: 'musi zawierać wartość w formacie "12 GBP", "54,95 CZK" itp.' }
  belongs_to :season
  has_many :team_rounds, dependent: :destroy
  has_many :teams, through: :team_rounds
  has_one :qualification, dependent: :destroy
  has_many :races, dependent: :destroy
  has_many :round_results, dependent: :destroy
  has_many :race_groups, dependent: :destroy
  has_many :carts, dependent: :destroy
end
