# frozen_string_literal: true

class RaceGroupTeam < ApplicationRecord
  belongs_to :race_group
  belongs_to :team
end
