# frozen_string_literal: true

class Season < ApplicationRecord
  validates_uniqueness_of :name, scope: :serie
  belongs_to :serie
  has_many :rounds, dependent: :destroy
  has_many :teams, dependent: :destroy
end
