# frozen_string_literal: true

class QualificationResult < ApplicationRecord
  belongs_to :qualification
  belongs_to :team
end
