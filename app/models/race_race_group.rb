# frozen_string_literal: true

class RaceRaceGroup < ApplicationRecord
  belongs_to :race
  belongs_to :race_group
end
