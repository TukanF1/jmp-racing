# frozen_string_literal: true

class RaceGroup < ApplicationRecord
  belongs_to :round
  has_many :race_group_teams, dependent: :destroy
  has_many :teams, through: :race_group_teams
  has_many :race_race_groups, dependent: :destroy
  has_many :races, through: :race_race_groups
end
