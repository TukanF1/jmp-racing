# frozen_string_literal: true

class Serie < ApplicationRecord
    validates_uniqueness_of :name
    has_many :seasons, dependent: :destroy
end
