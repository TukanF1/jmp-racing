# frozen_string_literal: true

class QualificationGroupResult < ApplicationRecord
  belongs_to :qualification_group
  belongs_to :team
  belongs_to :cart, optional: true
end
