# frozen_string_literal: true

class Cart < ApplicationRecord
  belongs_to :round
  has_many :qualification_group_results
end
