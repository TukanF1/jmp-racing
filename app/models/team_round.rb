# frozen_string_literal: true

class TeamRound < ApplicationRecord
  belongs_to :team
  belongs_to :round
end
