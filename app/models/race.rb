# frozen_string_literal: true

class Race < ApplicationRecord
  belongs_to :round
  has_many :race_race_groups, dependent: :destroy
  has_many :race_groups, through: :race_race_groups
  has_many :race_results, dependent: :destroy
end
