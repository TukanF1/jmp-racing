# frozen_string_literal: true

class RaceResult < ApplicationRecord
  belongs_to :race
  belongs_to :team
  belongs_to :cart, optional: true
end
