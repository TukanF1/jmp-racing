# frozen_string_literal: true

class Qualification < ApplicationRecord
  belongs_to :round
  has_many :qualification_groups, dependent: :destroy
  has_many :qualification_results, dependent: :destroy
end
