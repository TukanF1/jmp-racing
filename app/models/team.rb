# frozen_string_literal: true

class Team < ApplicationRecord
  belongs_to :season
  has_many :team_rounds
  has_many :rounds, through: :team_rounds
  has_many :qualification_group_results
  has_many :qualification_results
end
