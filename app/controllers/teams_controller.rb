# frozen_string_literal: true

class TeamsController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]
  before_action :set_team, only: [:edit, :update, :destroy]

  # GET /teams
  # GET /teams.json
  def index
    @season = Season.find(params[:season_id])
    @teams = @season.teams
  end

  # GET /teams/new
  def new
    @team = Team.new
  end

  # GET /teams/1/edit
  def edit
  end

  # POST /teams
  # POST /teams.json
  def create
    puts '------------------------------------------'
    puts team_params
    @team = Team.new(team_params)
    
    season = Season.find(params[:season_id])
    @team.season = season

    respond_to do |format|
      if @team.save
        format.html {
          redirect_to series_season_teams_path(season.serie, season),
          notice: 'Dodano zespół.'
        }
        format.json { render :show, status: :created, location: @team }
      else
        format.html { render :new }
        format.json { render json: @team.errors, status: :unprocessable_entity }
      end
    end
	rescue ActiveRecord::RecordNotUnique
		respond_to do |format|
			flash.now.alert = 'Zespół o tej nazwie już istnieje.'
			return render :new
		end
	rescue ActiveRecord::RecordInvalid
		respond_to do |format|
		  flash.now.alert = @season.errors.full_messages.join('<br />').html_safe
		  return render :new
		end
  rescue ActiveRecord::RecordNotFound
    redirect_to root_path
  end

  # PATCH/PUT /teams/1
  # PATCH/PUT /teams/1.json
  def update
    respond_to do |format|
      if params['team']['special_cart'].nil?
        params['team']['special_cart'] = 'false'
      end
      if @team.update(team_params)
        format.html {
          redirect_to series_season_teams_path(@team.season.serie, @team.season),
          notice: 'Zmieniono dane zespołu.'
        }
        format.json { render :show, status: :ok, location: @team }
      else
        format.html {
          flash.now.alert = @serie.errors.full_messages.join('<br />').html_safe
          render :edit
        }
        format.json { render json: @team.errors, status: :unprocessable_entity }
      end
    end
	rescue ActiveRecord::RecordNotUnique
		respond_to do |format|
			flash.now.alert = 'Zespół o tej nazwie już istnieje.'
			return render :edit
		end
	rescue ActiveRecord::RecordInvalid
		respond_to do |format|
		  flash.now.alert = @season.errors.full_messages.join('<br />').html_safe
		  return render :edit
		end
  end

  # DELETE /teams/1
  # DELETE /teams/1.json
  def destroy
    season = @team.season
    @team.destroy
    respond_to do |format|
      format.html {
        redirect_to series_season_teams_path(season.serie, season),
        notice: 'Zespół został usunięty.'
      }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_team
      @team = Team.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def team_params
      params.require(:team).permit(:name, :points, :special_cart, :season_id)
    end
end
