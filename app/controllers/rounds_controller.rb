# frozen_string_literal: true

class RoundsController < ApplicationController
  before_action :authenticate_user!, except: [:show, :edit_teams,
                                              :show_qualification, :show_races, :show_results, :show_carts]
  before_action :set_round, only: [:show, :edit, :update, :destroy,
                                   :edit_teams, :update_teams, :show_qualification,
                                   :edit_qualification_group_result, :update_qualification_group_result,
                                   :generate_qualification_result, :edit_qualification_result,
                                   :update_qualification_result, :show_races, :edit_race_result,
                                   :update_race_result, :show_results, :show_carts, :new_cart,
                                   :create_cart, :edit_cart, :update_cart, :destroy_cart]

  # GET /rounds/1
  # GET /rounds/1.json
  def show; end

  # GET /rounds/new
  def new
    @round = Round.new
    @round.start_time = Time.zone.now.strftime('%Y-%m-%d %H:%M')
    @round.price = '110 PLN'
    @round.max_number_of_carts = 10
  end

  # GET /rounds/1/edit
  def edit; end

  # POST /rounds
  # POST /rounds.json
  def create
    @round = Round.new(round_params)

    @round.season = Season.find(params[:season_id])

    respond_to do |format|
      if @round.save
        format.html {
          redirect_to series_season_round_path(
                        @round.season.serie, @round.season, @round
                      ),
                      notice: 'Dodano rundę.'
        }
        format.json { render :show, status: :created, location: @round }
      else
        format.html { render :new }
        format.json { render json: @round.errors, status: :unprocessable_entity }
      end
    end
  rescue ActiveRecord::RecordInvalid
    respond_to do |format|
      flash.now.alert = @season.errors.full_messages.join('<br />').html_safe
      return render :new
    end
  rescue ActiveRecord::RecordNotFound
    redirect_to root_path
  end

  # PATCH/PUT /rounds/1
  # PATCH/PUT /rounds/1.json
  def update
    respond_to do |format|
      if @round.update(round_params)
        format.html {
          redirect_to series_season_round_path(@round.season.serie, @round.season, @round),
                      notice: 'Zaktualizowano rundę.'
        }
        format.json { render :show, status: :ok, location: @round }
      else
        format.html {
          flash.now.alert = @round.errors.full_messages.join('<br />').html_safe
          render :edit
        }
        format.json { render json: @round.errors, status: :unprocessable_entity }
      end
    end
  rescue ActiveRecord::RecordNotUnique
    respond_to do |format|
      flash.now.alert = 'Runda o tej nazwie już istnieje.'
      return render :edit
    end
  rescue ActiveRecord::RecordInvalid
    respond_to do |format|
      flash.now.alert = @season.errors.full_messages.join('<br />').html_safe
      return render :edit
    end
  end

  # DELETE /rounds/1
  # DELETE /rounds/1.json
  def destroy
    season = @round.season
    serie = season.serie
    @round.destroy
    respond_to do |format|
      format.html {
        redirect_to series_season_path(serie, season),
                    notice: 'Runda została usunięta.'
      }
      format.json { head :no_content }
    end
  end

  def edit_teams
    @season_teams = @round.season.teams
    @teams = @round.teams
  end

  def update_teams
    params_teams = params[:teams]
    @round.season.teams.each do |season_team|
      if not(params_teams.nil?) &&
          params_teams.keys.include?(season_team.id.to_s)
        if ActiveRecord::Type::Boolean.new.cast(
              params_teams[season_team.id.to_s][:starting]
            ) && not(@round.teams.include? season_team)
          @round.teams << season_team
        end
      else
        @round.teams.delete(season_team)
      end
    end
    @round.save!

    respond_to do |format|
      format.html {
        redirect_to starting_teams_series_season_round_path(
                      @round.season.serie, @round.season, @round
                    ),
                    notice: 'Lista startowa została zaktualizowana.'
      }
      format.json { head :no_content }
    end
  end

  def show_qualification
    if @round.teams.size > 0
      if @round.qualification.nil? && user_signed_in?
        create_qualification
      end

      unless @round.qualification.nil?
        @qualification = @round.qualification

        @season = @round.season

        respond_to do |format|
          return render :qualification
        end
      end
    end

    if user_signed_in?
      flash[:alert] = 'Brak zgłoszonych zespołów.'
    end
    redirect_to starting_teams_series_season_round_path(
                  @round.season.serie, @round.season, @round
                )
  end

  def edit_qualification_group_result
    @qualification_group_result =
      QualificationGroupResult.find(params[:qualification_group_result_id])
  end

  def update_qualification_group_result
    @qualification_group_result =
      QualificationGroupResult.find(params[:qualification_group_result_id])
    time = params[:qualification_group_result][:time]
    @qualification_group_result.time = if time != ''
                                         if time.include? ':'
                                          '00:' + time
                                         else
                                           '00:00:' + time
                                         end
                                       else
                                         '00:00:00,000'
                                       end
    respond_to do |format|
      if @qualification_group_result.save
        format.html {
          redirect_to qualification_series_season_round_path(@round.season.serie, @round.season, @round),
                      notice: 'Zaktualizowano czas.'
        }
        format.json { render :show, status: :ok, location: @round }
      else
        format.html {
          flash.now.alert = @serie.errors.full_messages.join('<br />').html_safe
          render :edit_qualification_group_result
        }
        format.json { render json: @qualification_group_result.errors, status: :unprocessable_entity }
      end
    end
  end

  def generate_qualification_result
    qualification = @round.qualification
    qualification_results = []

    qualification.qualification_groups.each do |group|
      group.qualification_group_results.each do |result|
        qualification_result = QualificationResult.new
        qualification_result.qualification = qualification
        qualification_result.team = result.team
        qualification_result.time = result.time

        qualification_results << qualification_result
      end
    end

    qualification_results.sort_by! { |r| r.time.nil? ? Time.at(-60 * 60) : r.time }

    last_position = position = 1
    last_time = Time.at(-60 * 60)

    qualification_results.each do |result|
      if result.time == last_time
        result.position = last_position
      else
        last_position = result.position = position
      end
      position += 1
      last_time = result.time
    end

    qualification_results.each do |result|
      result.save!
    end

    flash[:success] = 'Wygenerowano wstępne wyniki.'
    redirect_to qualification_series_season_round_path(
                  @round.season.serie, @round.season, @round
                )
  end

  def edit_qualification_result
    @qualification_result =
      QualificationResult.find(params[:qualification_result_id])
  end

  def update_qualification_result
    @qualification_result =
      QualificationResult.find(params[:qualification_result_id])
    @qualification_result.position = params[:qualification_result][:position]
    time = params[:qualification_result][:time]
    @qualification_result.time = if time != ''
                                   if time.include? ':'
                                     '00:' + time
                                   else
                                     '00:00:' + time
                                   end
                                 else
                                   '00:00:00,000'
                                 end
    respond_to do |format|
      if @qualification_result.save
        format.html {
          redirect_to qualification_series_season_round_path(@round.season.serie, @round.season, @round),
                      notice: 'Zaktualizowano rezultat.'
        }
        format.json { render :show_qualification, status: :ok, location: @round }
      else
        format.html {
          flash.now.alert = @serie.errors.full_messages.join('<br />').html_safe
          render :edit_qualification_result
        }
        format.json { render json: @qualification_result.errors, status: :unprocessable_entity }
      end
    end
  end

  def show_races
    if not(@round.teams.empty?) && not(@round.qualification.nil?) && not(@round.qualification.qualification_results.empty?)
      if @round.race_groups.empty? && user_signed_in?
        create_race_groups
      end
      if not(@round.race_groups.empty?) && @round.races.empty? &&
          user_signed_in?
        create_races
      end

      unless @round.race_groups.empty?
        @races = @round.races

        @races_groups = @round.race_groups

        @season = @round.season

        respond_to do |format|
          return render :races
        end
      end
    end

    if user_signed_in?
      flash[:alert] = 'Brak zgłoszonych zespołów.'
    end
    redirect_to starting_teams_series_season_round_path(
                  @round.season.serie, @round.season, @round
                )
  end

  def edit_race_result
    @race_result = RaceResult.find(params[:race_result_id])
  end

  def update_race_result
    @race_result = RaceResult.find(params[:race_result_id])
    @race_result.points = params[:race_result][:points]
    respond_to do |format|
      if @race_result.save
        set_positions_for_race_results
        format.html {
          redirect_to races_series_season_round_path(@round.season.serie, @round.season, @round),
                      notice: 'Zaktualizowano wynik.'
        }
        format.json { render :races, status: :ok, location: @round }
      else
        format.html {
          flash.now.alert = @serie.errors.full_messages.join('<br />').html_safe
          render :edit_race_result
        }
        format.json { render json: @race_result.errors, status: :unprocessable_entity }
      end
    end
  end

  def show_results
    if @round.round_results.empty?
      if user_signed_in?
        generate_round_results
      else
        redirect_to root_path
      end
    end
    @season = @round.season
  end

  def show_carts; end

  def new_cart
    @cart = Cart.new
    @cart.round = @round
    @cart.number = 1
    @cart.special = false
  end

  def create_cart
    @cart = Cart.new(cart_params)

    @cart.round = @round

    if params['cart']['special'].nil?
      params['cart']['special'] = 'false'
    end

    respond_to do |format|
      if @cart.save
        format.html {
          redirect_to carts_series_season_round_path(
                        @round.season.serie, @round.season, @round
                      ),
                      notice: 'Dodano gokarta.'
        }
        format.json { render :show_carts, status: :created, location: @round }
      else
        format.html { render :new_cart }
        format.json { render json: @cart.errors, status: :unprocessable_entity }
      end
    end
  rescue ActiveRecord::RecordNotUnique
    respond_to do |format|
      flash.now.alert = 'Gokart z tym numerem już istnieje.'
      return render :new_cart
    end
  rescue ActiveRecord::RecordInvalid
    respond_to do |format|
      flash.now.alert = @season.errors.full_messages.join('<br />').html_safe
      return render :new_cart
    end
  rescue ActiveRecord::RecordNotFound
    redirect_to root_path
  end

  def edit_cart
    @cart = Cart.find(params[:cart_id])
  rescue ActiveRecord::RecordNotFound
    redirect_to root_path
  end

  def update_cart
    @cart = Cart.find(params[:cart_id])

    if params['cart']['special'].nil?
      params['cart']['special'] = 'false'
    end

    respond_to do |format|
      if @cart.update(cart_params)
        format.html {
          redirect_to carts_series_season_round_path(
                        @round.season.serie, @round.season, @round
                      ),
                      notice: 'Zaktualizowano gokarta.'
        }
        format.json { render :show_carts, status: :ok, location: @round }
      else
        format.html {
          flash.now.alert = @cart.errors.full_messages.join('<br />').html_safe
          render :edit_cart
        }
        format.json { render json: @cart.errors, status: :unprocessable_entity }
      end
    end
  rescue ActiveRecord::RecordNotUnique
    respond_to do |format|
      flash.now.alert = 'Gokart z tym numerem już istnieje.'
      return render :edit_cart
    end
  rescue ActiveRecord::RecordInvalid
    respond_to do |format|
      flash.now.alert = @season.errors.full_messages.join('<br />').html_safe
      return render :edit_cart
    end
  end

  def destroy_cart
    @cart = Cart.find(params[:cart_id])
    season = @round.season
    serie = season.serie
    @cart.destroy
    respond_to do |format|
      format.html {
        redirect_to carts_series_season_round_path(serie, season, @round),
                    notice: 'Gokart został usunięty.'
      }
      format.json { head :no_content }
    end
  rescue ActiveRecord::RecordNotFound
    redirect_to root_path
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_round
    @round = Round.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    redirect_to root_path
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def round_params
    params.require(:round).permit(
      :name, :track_name, :address, :start_time, :price, :max_number_of_carts,
      :ended_races_counter, :season_id
    )
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def cart_params
    params.require(:cart).permit(
      :number, :special, :round_id
    )
  end

  def create_qualification
    @qualification = Qualification.new
    @qualification.name = 'Kwalifikacje'
    @qualification.round = @round
    @qualification.save!
    number_of_qualification_groups =
      (@round.teams.size.to_f / @round.max_number_of_carts.to_f).ceil

    alphabet = (10...36).map { |i| (i.to_s 36).upcase }

    teams_in_one_group =
      (@round.teams.size.to_f / number_of_qualification_groups.to_f).floor

    shuffled_teams = @round.teams.shuffle

    for i in 0..number_of_qualification_groups - 1
      qualification_group = QualificationGroup.new
      qualification_group.name = 'Grupa ' + alphabet[i]
      qualification_group.qualification = @qualification
      qualification_group.save!

      for j in 0..teams_in_one_group - 1
        qualification_group_result = QualificationGroupResult.new
        qualification_group_result.qualification_group = qualification_group
        qualification_group_result.team = shuffled_teams[i * teams_in_one_group + j]
        qualification_group_result.time = Time.at(-60 * 60)
        qualification_group_result.save!
      end
    end
    qualification_groups = @qualification.qualification_groups
    a = (number_of_qualification_groups - 1) * teams_in_one_group + teams_in_one_group
    b = shuffled_teams.size - 1
    for i in a..b
      qualification_group_result = QualificationGroupResult.new
      qualification_group_result.qualification_group = qualification_groups[i - a]
      qualification_group_result.team = shuffled_teams[i]
      qualification_group_result.time = Time.at(-60 * 60)
      qualification_group_result.save!
    end

    qualification_groups.each do |group|
      gr_special_teams = []
      gr_other_teams = []
      group.qualification_group_results.each do |group_result|
        if group_result.team.special_cart
          gr_special_teams << group_result
        else
          gr_other_teams << group_result
        end
      end
      gr_special_teams.shuffle!
      gr_other_teams.shuffle!

      special_carts = []
      carts = []
      @round.carts.each do |cart|
        if cart.special
          special_carts << cart
        else
          carts << cart
        end
      end
      special_carts.shuffle!
      carts.shuffle!

      gr_special_teams.each do |gr_special_team|
        gr_special_team.cart = special_carts.pop
        if gr_special_team.cart.nil?
          gr_special_team.cart = carts.pop
        end
        gr_special_team.save!
      end
      carts += special_carts
      carts.shuffle!
      gr_other_teams.each do |gr_other_team|
        gr_other_team.cart = carts.pop
        gr_other_team.save!
      end
    end
  end

  def create_race_groups
    number_of_races_groups =
      (2.0 * @round.teams.size.to_f / @round.max_number_of_carts.to_f).ceil

    if number_of_races_groups == 2
      number_of_races_groups = 1
    end

    if @round.teams.size % number_of_races_groups > 1
      number_of_races_groups += 1
    end

    alphabet = (10...36).map { |i| (i.to_s 36).upcase }

    qualification_results = @round.qualification.qualification_results.sort_by {
      |r| r.position
    }

    for i in 0..number_of_races_groups - 1
      race_group = RaceGroup.new
      race_group.name = 'Grupa ' + alphabet[i]
      race_group.round = @round
      race_group.save!
    end

    for i in 0..qualification_results.size - 1
      race_group_team = RaceGroupTeam.new
      race_group_team.race_group = @round.race_groups[i % number_of_races_groups]
      race_group_team.team = qualification_results[i].team
      race_group_team.save!
    end
  end

  def create_races
    groups = @round.race_groups
    groups_number = groups.size

    if groups_number == 1
      group = groups[0]

      race = Race.new
      race.name = group.name
      race.round = @round
      race.save!

      group.races << race
      group.save!

      group.teams.each do |team|
        race_result = RaceResult.new
        race_result.race = race
        race_result.team = team
        race_result.position = 1
        race_result.points = 0
        race_result.save!
      end
    else
      for i in 0..groups_number - 1
        for j in i + 1..groups_number - 1
          group_a = groups[i]
          group_b = groups[j]

          race = Race.new
          race.name = group_a.name + ' - ' + group_b.name
          race.round = @round
          race.save!

          group_a.races << race
          group_b.races << race

          group_a.save!
          group_b.save!

          group_a.teams.each do |team|
            race_result = RaceResult.new
            race_result.race = race
            race_result.team = team
            race_result.position = 1
            race_result.points = 0
            race_result.save!
          end

          group_b.teams.each do |team|
            race_result = RaceResult.new
            race_result.race = race
            race_result.team = team
            race_result.position = 1
            race_result.points = 0
            race_result.save!
          end
        end
      end
    end

    @round.races.each do |race|
      r_special_teams = []
      r_other_teams = []
      race.race_results.each do |race_result|
        if race_result.team.special_cart
          r_special_teams << race_result
        else
          r_other_teams << race_result
        end

        race_result.starting_position = @round.qualification.qualification_results.select {
          |r| r.team.id == race_result.team.id
        }.first.position
        race_result.save!
      end

      race_results = race.race_results.sort_by { |r| r.starting_position }
      for i in 1..race_results.size
        race_result = race_results[i - 1]
        race_result.starting_position = i
        race_result.save!
      end

      r_special_teams.shuffle!
      r_other_teams.shuffle!

      special_carts = []
      carts = []
      @round.carts.each do |cart|
        if cart.special
          special_carts << cart
        else
          carts << cart
        end
      end
      special_carts.shuffle!
      carts.shuffle!

      r_special_teams.each do |r_special_team|
        r_special_team.cart = special_carts.pop
        if r_special_team.cart.nil?
          r_special_team.cart = carts.pop
        end
        r_special_team.save!
      end
      carts += special_carts
      carts.shuffle!
      r_other_teams.each do |r_other_team|
        r_other_team.cart = carts.pop
        r_other_team.save!
      end
    end
  end

  def set_positions_for_race_results
    last_position = position = 1
    last_points = nil
    @race_result.race.race_results.sort_by { |r| r.points }.each do |result|
      if last_points == result.points
        result.position = last_position
      else
        last_position = result.position = position
      end
      position += 1
      last_points = result.points
      result.save!
    end
  end

  def generate_round_results
    @round.teams.each do |team|
      result = RoundResult.new
      @round.round_results << result
      result.team = team
      result.points = 0
    end

    @round.races.each do |race|
      race.race_results.each do |race_result|
        round_result = @round.round_results.filter {
          |r| r.team == race_result.team
        }.first
        unless round_result.nil?
          round_result.points += race_result.points
        end
      end
    end

    last_position = position = 1
    last_points = nil
    @round.round_results.sort_by { |r| r.points }.each do |result|
      if last_points == result.points
        result.position = last_position
      else
        last_position = result.position = position
      end
      position += 1
      last_points = result.points
    end

    loop do
      was_draw = false
      last_position = nil
      last_result = nil
      @round.round_results.sort_by { |r| r.position }.each do |result|
        if last_position == result.position
          was_draw = true
          result_a = last_result
          result_b = result
          qual_a = @round.qualification.qualification_results.find_by(team: result_a.team)
          qual_b = @round.qualification.qualification_results.find_by(team: result_b.team)

          if qual_a.position < qual_b.position
            result_b.position += 1
          else
            result_a.position += 1
          end
          last_position = [last_result.position, result.position].min
          if last_result.position != last_position
            last_result = result
          end
        else
          last_position = result.position
          last_result = result
        end
      end
      break unless was_draw
    end

    @round.round_results.each do |result|
      result.save!
    end
  end
end
