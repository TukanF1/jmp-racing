# frozen_string_literal: true

class SeriesController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]
  before_action :set_series, only: [:show, :edit, :update, :destroy]

  # GET /series
  # GET /series.json
  def index
    @content_0 = '<div class="d-flex justify-content-between">' +
        '<h5 class="mb-1">'
    @content_1 = '</h5><small style="allign: right;">Zakończone sezony: '
    @content_2 = '</small></div>'
    @content_3 = '<small>Liczba sezonów: '
    @content_4 = '</small>'
    @series = Serie.all.sort_by { |s| s.name.downcase }
  end

  # GET /series/1
  # GET /series/1.json
  def show
    @seasons = @serie.seasons.sort_by { |s| s.name.downcase }
    
    @content_0 = '<div class="d-flex justify-content-between">' +
        '<h5 class="mb-1">'
    @content_1 = '</h5><small style="allign: right;">Zakończone rundy: '
    @content_2 = '</small></div>'
    @content_3 = '<small>Liczba rund: '
    @content_4 = '</small>'
  end

  # GET /series/new
  def new
    @serie = Serie.new
  end

  # GET /series/1/edit
  def edit
  end

  # POST /series
  # POST /series.json
  def create
    begin
      @serie = Serie.new(series_params)
  
      respond_to do |format|
        if @serie.save
          format.html { redirect_to @serie, notice: 'Stworzono nową serię.' }
          format.json { render :show, status: :created, location: @serie }
        else
          format.html { render :new }
          format.json { render json: @serie.errors, status: :unprocessable_entity }
        end
      end
		rescue ActiveRecord::RecordNotUnique
			respond_to do |format|
				flash.now.alert = 'Seria o tej nazwie już istnieje.'
				return render :new
			end
		rescue ActiveRecord::RecordInvalid
			respond_to do |format|
			  flash.now.alert = @season.errors.full_messages.join('<br />').html_safe
			 # @form_class = "needs-validation was-validated"
			  return render :new
			end
		end
  end

  # PATCH/PUT /series/1
  # PATCH/PUT /series/1.json
  def update
    begin
      respond_to do |format|
        if @serie.update(series_params)
          format.html { redirect_to @serie, notice: 'Zmieniono nazwę serii.' }
          format.json { render :show, status: :ok, location: @serie }
        else
          format.html {
            flash.now.alert = @serie.errors.full_messages.join('<br />').html_safe
            render :edit
          }
          format.json { render json: @serie.errors, status: :unprocessable_entity }
        end
      end
		rescue ActiveRecord::RecordNotUnique
			respond_to do |format|
				flash.now.alert = 'Seria o tej nazwie już istnieje.'
				return render :edit
			end
		rescue ActiveRecord::RecordInvalid
			respond_to do |format|
			  flash.now.alert = @season.errors.full_messages.join('<br />').html_safe
			  return render :edit
			end
		end
  end

  # DELETE /series/1
  # DELETE /series/1.json
  def destroy
    @serie.destroy
    respond_to do |format|
      format.html { redirect_to series_index_url, notice: 'Usunięto serię.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_series
      @serie = Serie.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def series_params
      params.require(:serie).permit(:name)
    end
end
