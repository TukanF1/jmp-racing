# frozen_string_literal: true

class SeasonsController < ApplicationController
  before_action :authenticate_user!, except: [:show]
  before_action :set_season, only: [:show, :edit, :update, :destroy]

  # GET /seasons/1
  # GET /seasons/1.json
  def show
    @rounds = @season.rounds
    
    @content_0 = '<div class="d-flex justify-content-between">' +
        '<h5 class="mb-1">'
    @content_1 = '</h5><small style="allign: right;">Termin: '
    @content_2 = '</small></div><p class="mb-1">'
    @content_3 = '</p><small>Cena: '
    @content_4 = '</small>'
  end

  # GET /seasons/new
  def new
    @season = Season.new
  end

  # GET /seasons/1/edit
  def edit
  end

  # POST /seasons
  # POST /seasons.json
  def create
    @season = Season.new(season_params)
    
    @season.serie = Serie.find(params[:series_id])
    
    respond_to do |format|
      if @season.save
        format.html {
          redirect_to series_season_path(@season.serie, @season),
          notice: 'Dodano sezon.'
        }
        format.json {
          render :show,
          status: :created,
          location: @season
        }
      else
        format.html { render :new }
        format.json { render json: @season.errors, status: :unprocessable_entity }
      end
    end
	rescue ActiveRecord::RecordInvalid
		respond_to do |format|
		  flash.now.alert = @season.errors.full_messages.join('<br />').html_safe
		  return render :new
		end
  rescue ActiveRecord::RecordNotFound
    redirect_to root_path
  end

  # PATCH/PUT /seasons/1
  # PATCH/PUT /seasons/1.json
  def update
    respond_to do |format|
      if @season.update(season_params)
        format.html {
          redirect_to series_season_path(@season.serie, @season),
          notice: 'Zmieniono nazwę sezonu.'
        }
        format.json { render :show, status: :ok, location: @season }
      else
        format.html {
          flash.now.alert = @serie.errors.full_messages.join('<br />').html_safe
          render :edit
        }
        format.json { render json: @season.errors, status: :unprocessable_entity }
      end
    end
	rescue ActiveRecord::RecordNotUnique
		respond_to do |format|
			flash.now.alert = 'Sezon o tej nazwie już istnieje.'
			return render :edit
		end
	rescue ActiveRecord::RecordInvalid
		respond_to do |format|
		  flash.now.alert = @season.errors.full_messages.join('<br />').html_safe
		  return render :edit
		end
	end

  # DELETE /seasons/1
  # DELETE /seasons/1.json
  def destroy
    serie = @season.serie
    @season.destroy
    respond_to do |format|
      format.html {
        redirect_to series_path(serie),
        notice: 'Sezon został usunięty.'
      }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_season
      @season = Season.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def season_params
      params.require(:season).permit(:name)
    end
end
